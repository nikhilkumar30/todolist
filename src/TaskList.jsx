import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Box,
  Text,
  Button,
  Flex,
  Spacer,
  Tooltip,
  Input,
} from "@chakra-ui/react";
import { DeleteIcon, EditIcon, CheckIcon, CloseIcon } from "@chakra-ui/icons";
import { deleteTask, updateTask } from "./taskSlice";

function TaskList() {
  const [editedTaskText, setEditedTaskText] = useState("");
  const [editingTaskId, setEditingTaskId] = useState(null);
  const [completedTaskIds, setCompletedTaskIds] = useState([]);
  const tasks = useSelector((state) => state.tasks || []);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedCompletedTaskIds = localStorage.getItem("completedTaskIds");
    if (storedCompletedTaskIds) {
      setCompletedTaskIds(JSON.parse(storedCompletedTaskIds));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("completedTaskIds", JSON.stringify(completedTaskIds));
  }, [completedTaskIds]);

  const handleDelete = (taskId) => {
    dispatch(deleteTask(taskId));
  };

  const handleEdit = (taskId, taskText) => {
    setEditingTaskId(taskId);
    setEditedTaskText(taskText);
  };

  const handleUpdate = (taskId) => {
    if (editedTaskText.trim() !== "") {
      dispatch(updateTask({ id: taskId, text: editedTaskText }));
      setEditingTaskId(null);
      setEditedTaskText("");
    }
  };

  const handleCancel = () => {
    setEditingTaskId(null);
    setEditedTaskText("");
  };

  const handleToggleCompletion = (taskId) => {
    if (completedTaskIds.includes(taskId)) {
      setCompletedTaskIds((prevIds) => prevIds.filter((id) => id !== taskId));
    } else {
      setCompletedTaskIds((prevIds) => [...prevIds, taskId]);
    }
  };

  const isEditing = (taskId) => {
    return taskId === editingTaskId;
  };

  const isCompleted = (taskId) => {
    return completedTaskIds.includes(taskId);
  };

  return (
    <Box mt={4} w="80%" bg="#8758ff" p={4} borderRadius="md">
      {tasks.map((task) => (
        <Flex key={task.id} align="center" mb={2}>
          <Text
            color={isCompleted(task.id) ? "gray.500" : "white"}
            flexGrow="1"
            textDecoration={isCompleted(task.id) ? "line-through" : "none"}
            onClick={() =>
              !isEditing(task.id) && handleToggleCompletion(task.id)
            }
            cursor={!isEditing(task.id) ? "pointer" : "default"}
          >
            {task.text}
          </Text>
          <Spacer />
          {isEditing(task.id) ? (
            <>
              <Input
                value={editedTaskText}
                onChange={(e) => setEditedTaskText(e.target.value)}
                autoFocus
                mr={2}
              />
              <Tooltip label="Update" placement="top">
                <Button
                  colorScheme="green"
                  onClick={() => handleUpdate(task.id)}
                  leftIcon={<CheckIcon />}
                  mr={2}
                />
              </Tooltip>
              <Tooltip label="Cancel" placement="top">
                <Button
                  colorScheme="gray"
                  onClick={handleCancel}
                  leftIcon={<CloseIcon />}
                />
              </Tooltip>
            </>
          ) : (
            <>
              <Tooltip label="Edit" placement="top">
                <Button
                  colorScheme="blue"
                  onClick={() => handleEdit(task.id, task.text)}
                  leftIcon={<EditIcon />}
                  mr={2}
                />
              </Tooltip>
              <Tooltip label="Delete" placement="top">
                <Button
                  colorScheme="red"
                  onClick={() => handleDelete(task.id)}
                  leftIcon={<DeleteIcon />}
                />
              </Tooltip>
            </>
          )}
        </Flex>
      ))}
    </Box>
  );
}

export default TaskList;
