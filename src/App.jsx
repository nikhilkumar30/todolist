import React from "react";
import { Provider } from "react-redux";
import {
  ChakraProvider,
  Center,
  Heading,
  VStack,
  Box,
  Flex,
} from "@chakra-ui/react";
import store from "./redux/store";
import TaskInput from "./TaskInput";
import TaskList from "./TaskList";

function App() {
  return (
    <Provider store={store}>
      <ChakraProvider>
        <Flex
          bg="#8758ff"
          minH="100vh"
          justifyContent="center"
          alignItems="center"
        >
          <Box bg="#1A1A40"  borderRadius="md"maxW={{ base: "100%", md: "80%", lg: "60%" }} mx="auto" px={4} p={{ base: "6", md: "8" }}>
            <Center>
              <VStack spacing={8} align="center">
                <Heading as="h1" size="xl" color="white">
                  To Do List
                </Heading>
                <TaskInput />
                <TaskList />
              </VStack>
            </Center>
          </Box>
        </Flex>
      </ChakraProvider>
    </Provider>
  );
}

export default App;
